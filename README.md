* これは何  

mikutter 起動時にろき氏をブロックします。  
既にブロックしているかどうか、ブロックするかどうかを確認せずにブロックリクエストを送ります。

* 注意  

むやみにブロックするのはやめましょう。

* ライセンス
[NYSL]( http://www.kmonos.net/nysl/) に従います。
